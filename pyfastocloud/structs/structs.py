class OperationSystem(object):
    __slots__ = ["name", "version", "arch"]

    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, value)

    def __str__(self):
        return "{0} {1}({2})".format(self.name, self.version, self.arch)

    def to_dict(self):
        return {"name": self.name, "version": self.version, "arch": self.arch}

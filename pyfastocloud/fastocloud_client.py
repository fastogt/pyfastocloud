
class Commands:
    # stream
    START_STREAM_COMMAND = "start_stream"
    STOP_STREAM_COMMAND = "stop_stream"
    CLEAN_STREAM_COMMAND = "clean_stream"
    RESTART_STREAM_COMMAND = "restart_stream"
    GET_LOG_STREAM_COMMAND = "get_log_stream"
    GET_PIPELINE_STREAM_COMMAND = "get_pipeline_stream"
    GET_CONFIG_JSON_STREAM_COMMAND = "get_config_json_stream"
    CHANGE_INPUT_STREAM_COMMAND = "change_input_stream"

    WEBRTC_OUT_INIT_STREAM = "webrtc_out_init_stream"
    WEBRTC_OUT_SDP_STREAM = "webrtc_out_sdp_stream"
    WEBRTC_OUT_ICE_STREAM = "webrtc_out_ice_stream"
    WEBRTC_OUT_DEINIT_STREAM = "webrtc_out_deinit_stream"

    WEBRTC_IN_INIT_STREAM = "webrtc_in_init_stream"
    WEBRTC_IN_SDP_STREAM = "webrtc_in_sdp_stream"
    WEBRTC_IN_ICE_STREAM = "webrtc_in_ice_stream"
    WEBRTC_IN_DEINIT_STREAM = "webrtc_in_deinit_stream"

    INJECT_MASTER_INPUT_URL = "inject_master_input_url"
    REMOVE_MASTER_INPUT_URL = "remove_master_input_url"

    # service
    GET_HARDWARE_HASH_COMMAND = "get_hardware_key"
    GET_STATS_COMMAND = "stats"
    GET_LOG_SERVICE_COMMAND = "get_log_service"
    PROBE_IN_STREAM_COMMAND = "probe_in_stream"
    PROBE_OUT_STREAM_COMMAND = "probe_out_stream"
    MOUNT_S3_BUCKET = "mount_s3_bucket"
    UNMOUNT_S3_BUCKET = "unmount_s3_bucket"
    SCAN_FOLDER_COMMAND = "scan_folder"
    SCAN_S3_BUCKETS_COMMAND = "scan_s3_buckets"

    # broadcast
    CHANGED_STREAM_BROADCAST = "changed_source_stream"
    STATISTIC_STREAM_BROADCAST = "statistic_stream"
    ML_NOTIFICATION_STREAM_BROADCAST = "ml_notification_stream"
    RESULT_STREAM_BROADCAST = "result_stream"
    QUIT_STATUS_STREAM_BROADCAST = "quit_status_stream"

    WEBRTC_OUT_INIT_STREAM_BROADCAST = "webrtc_out_init_stream"
    WEBRTC_OUT_SDP_STREAM_BROADCAST = "webrtc_out_sdp_stream"
    WEBRTC_OUT_ICE_STREAM_BROADCAST = "webrtc_out_ice_stream"
    WEBRTC_OUT_DEINIT_STREAM_BROADCAST = "webrtc_out_deinit_stream"

    WEBRTC_IN_INIT_STREAM_BROADCAST = "webrtc_in_init_stream"
    WEBRTC_IN_SDP_STREAM_BROADCAST = "webrtc_in_sdp_stream"
    WEBRTC_IN_ICE_STREAM_BROADCAST = "webrtc_in_ice_stream"
    WEBRTC_IN_DEINIT_STREAM_BROADCAST = "webrtc_in_deinit_stream"
    # broadcast
    STATISTIC_SERVICE_BROADCAST = "statistic_service"

    # broadcast
    STATISTIC_CDN_BROADCAST = "statistic_cdn"
